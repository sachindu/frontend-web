import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import CustomerHome from './components/CustomerHome';
import CustomerSignin from './components/CustomerSignin';
import CustomerSignup from './components/CustomerSignup';
import Home from './components/Home';
import './css/style.css';

function App() {
  return (
    <Router>
      <div className="App" style={{height:'100vh'}}>
        <Switch>
          <Route exact path='/'>
            <Home/>
          </Route>
          <Route exact path='/customer/signin'>
            <CustomerSignin/>
          </Route>
          <Route exact path='/customer/signup'>
            <CustomerSignup/>
          </Route>
          <Route path='/customer/home'>
            <CustomerHome/>
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
